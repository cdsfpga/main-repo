#!/usr/bin/env python3
import argparse
import logging
import os
import sys
from io import FileIO, StringIO
import xml.etree.ElementTree as ET
from base64 import b64encode, b64decode
import xml.sax
import tempfile

VERBOSE_LEVELS = {0: 50, 1:40, 2: 20, 3: 10}
prog_description = '''Convert input file to XML using a known schema'''
#
# bin2xml -i myfile.bin -o
# xmlbinex bin2xml -i ipath [-o opath]
# xmlbinex xml2bin -i ipath [-o opath] - opath must be a directory
#
'''
We are going to take a input file and create the following file XML schema 
file for it:

<file>
  <metadata>
    <name>input.bin</name>
    <length>2096</length> 
    <file-type></file-type>
  </metadata>
  <blocks>
    <block length="1024"> base64 encoded block
    </block>
    <block length="1024">
       base64 encoded block
    </block>
  </blocks>
</file>
'''
BLOCK_SIZE = 512*3 
class bin2xml:
    ''' The document is written by pieces to allow an arbitrary file size. There 
        might be a 1TB file that will not fit in memory.
    '''
    def __call__(self, args):
        if not args.output:
            args.output = open(args.input.name + '.xml', 'wb') 

        logging.info('input: %s\noutput: %s\nbs: %d', args.input.name, args.output.name, args.block_size)
        self._input = args.input
        self._output = args.output
        self._block_size = args.block_size
        # 
        # We need the size to calculate the number of blocks we are transmitting.
        # That, assuming it is not a sparse file.
        #
        self._file_size = 0
        try:
            self._start_doc()
            self._add_metadata()
            self._add_blocks()
            self._end_doc()
            self._output.close()

        except Exception as error:
            logging.error('Error: %s', str(error))
            self._output.close()
            try:
                os.unlink(self._output.name)

            except IOError:
                log.error('error: unable to remove partial output')

            raise error

    def _start_doc(self):
        start = b'''<?xml version="1.0" encoding="ASCII" ?>
<!-- put here xml schema definition blurb -->
<file>
 '''
        self._output.write(start)

    def _add_metadata(self):
        metadata_doc = ET.Element('metadata')
        el_name = ET.SubElement(metadata_doc, 'name', {'encoding': 'base64Binary'})
        el_name.text = b64encode(self._input.name.encode('utf-8')).decode()
        el_size = ET.SubElement(metadata_doc, 'size')
        self._file_size = os.stat(self._input.name).st_size
        el_size.text = '%d' % self._file_size
        self._output.write(ET.tostring(metadata_doc))

    def _get_block_count(self): 
        return int(self._file_size/self._block_size) + (1 if self._file_size % self._block_size else 0)

    def _add_blocks(self):
        start_blocks = b'''
  <blocks encoding="base64Binary" count="%s">
   ''' % str(self._get_block_count()).encode()
        self._output.write(start_blocks)
        # 
        # the value of offset is lagging behind the iteration and adjusted based
        # on the read size
        #
        offset = 0
        for block in iter(lambda: self._input.read(self._block_size), b''):
            block_metadata = {
                'offset': '%d' % offset,
                'size':  '%d' % len(block) 
            }
            block_element = ET.Element('block', block_metadata)
            offset += len(block) 
            block_element.text = b64encode(block).decode()
            self._output.write(ET.tostring(block_element))
            self._output.write(b'\n')

        end_blocks = b''' </blocks>'''
        self._output.write(end_blocks)

    def _end_doc(self):
        end = b'''
</file>
'''
        self._output.write(end)

class FileHandler(xml.sax.ContentHandler):
    def __init__(self, output, temp_file):
        self._output = output
        self._tree_stack = []
        self._temp_filename = '' if not temp_file else temp_file[1]
        self._filename = ''
        self._buffer = StringIO()

    def _start_block(self, attrs):
        logging.debug('processing block')
        self._output.seek(int(attrs['offset']))

    def _end_block(self):
        self._buffer.seek(0)
        self._output.write(b64decode(self._buffer.read()))
        self._buffer.seek(0)
        self._buffer.truncate()

    def _end_blocks(self):
        self._output.close()
        if self._temp_filename:
            logging.info("source: %s, dest: %s" % (self._output.name, self._filename))
            os.rename(self._temp_filename, self._filename)

    def characters(self, data):
        logging.debug('received data for %s' % self._tree_stack[-1])
        if self._tree_stack[-1] == 'block':
            self._buffer.write(data)
            logging.debug('block: >%s<', data)

        elif self._tree_stack[-1] == 'name':
            self._filename = b64decode(data).decode('utf-8')

    def startElement(self, name, attrs):
        logging.debug('startElement %s', name)
        self._tree_stack.append(name)
        method = getattr(self, '_start_%s' % name, lambda attrs: None)
        method(attrs)

    def endElement(self, name):
        self._tree_stack.pop()
        method = getattr(self, '_end_%s' % name, lambda: None)
        method()

class xml2bin:
    ''' cds xml2bin -i ipath [-o opath]
    '''
    def __call__(self, args):
        temp_file = ()
        if not args.output:
            temp_file = tempfile.mkstemp()
            self._output = os.fdopen(temp_file[0], 'wb')

        else:
            self._output = args.output

        parser = xml.sax.make_parser()
        parser.setContentHandler(FileHandler(self._output, temp_file))
        parser.parse(args.input)

def setup_logging():
    logging.basicConfig(format='%(message)s', level=logging.DEBUG)

def main():
    ''' We have 2 subparsers, one for bin2xml and another for xml2bin
    '''
    parser = argparse.ArgumentParser(prog='cds', description=prog_description)
    subparsers = parser.add_subparsers(title='subcommands')
    #
    # bin2xml
    #
    bin2xml_parser = subparsers.add_parser('bin2xml', aliases=['b2x'])
    bin2xml_parser.add_argument('--input', '-i', 
                         required=True, 
                         metavar='ipath', 
                         action='store', 
                         type=argparse.FileType('rb', 0), 
                         help='input file or directory')
    bin2xml_parser.add_argument('--output', '-o', 
                         metavar='opath', 
                         action='store', 
                         type=argparse.FileType('wb', 0), 
                         default=None,
                         help='output file or directory. default is <input>.xml')
    bin2xml_parser.add_argument('--block-size', 
                         metavar='block_size', 
                         action='store', 
                         type=int, 
                         default=BLOCK_SIZE,
                         help='output file or directory. default is <input>.xml')
    bin2xml_parser.add_argument('--verbose', '-v', 
                         action='count', 
                         help='verbose level')

    bin2xml_parser.set_defaults(func=bin2xml())

    #
    # xml2bin
    #
    xml2bin_parser = subparsers.add_parser('xml2bin', aliases=['b2x'])
    xml2bin_parser.add_argument('--input', '-i', 
                         required=True, 
                         metavar='ipath', 
                         action='store', 
                         type=argparse.FileType('rb', 0), 
                         help='input file or directory')
    xml2bin_parser.add_argument('--output', '-o', 
                         metavar='opath', 
                         action='store', 
                         type=argparse.FileType('wb', 0), 
                         default=None,
                         help='output file or directory. default is <input>.xml')
    xml2bin_parser.add_argument('--block-size', 
                         metavar='block_size', 
                         action='store', 
                         type=int, 
                         default=BLOCK_SIZE,
                         help='output file or directory. default is <input>.xml')
    xml2bin_parser.add_argument('--verbose', '-v', 
                         action='count', 
                         default=0,
                         help='verbose level')
    xml2bin_parser.set_defaults(func=xml2bin())
    args = parser.parse_args()
    args.verbose = args.verbose or 0
    logging.getLogger().setLevel(VERBOSE_LEVELS[3 if args.verbose > 3 else args.verbose])
    #
    # The work is done through the subcommand function, namely __call__ method.
    #
    args.func(args)
    
def setup():
    setup_logging()

def teardown():
    pass

if __name__ == '__main__':
    try:
        setup()
        main()

    finally:
        teardown()

