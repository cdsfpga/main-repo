#Usage for binaries in the src directory:
## `cds` 

Converts files from binary format to XML and viceversa. XML files follow the schema defined in `cds.xsd`. All files are read as binary files when converting to XML, and all files are written as binary when converting from XML to binary.

The output of the full round trip process, that is from binary to XML and then from XML to binary, is a file that replicates the original file. Files are converted to XML for transpose purposes and once they are transported they are converted back to binary.

Usage:
The cds program has 2 subcommands, `bin2xml` and `xml2bin`. Both subcommands have the same same set of arguments.

- `-i, --input input-file` - required input file
- `-o, --output output-file` -  optional output file
- `-v, --verbose` - cummulative argument for verbose level

###`$ cds bin2xml -i inputfile.bin [-o output-file.xml]`

Converts a binary file to XML. When the `output` argument is not provided the `.xml` extension is added to the input path.

###`$ cds xml2bin -i input.file [-o output-file.bin]`

Converts a file back to its original binary format. When the `output` argument is not provided the `<name/>` XML value is used.